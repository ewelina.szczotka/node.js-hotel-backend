const Reservation = require('../models/reservation');
const Room = require('../models/room');
const mongoose = require('mongoose');

exports.reservations_get_all = (req,res,next) => {
    Reservation.find()
    .select('-__v')
    .populate('room', '_id')
    .exec()
    .then(docs => {
        res.status(200).json({
            count: docs.length,
            reservations: docs.map(doc => {
                return {
                    id: doc._id,
                    room: doc.room,
                    paid: doc.paid,
                    beginDate: doc.beginDate,
                    endDate: doc.endDate,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/reservations/' + doc._id
                    }
                }
            })
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
}

exports.reservations_post = (req,res,next) => {
    Room.findById(req.body.roomId)
        .then(room => {
            if(!room) {
                return res.status(404).json({
                    message: 'Room not found.'
                });
            }
            const reservation = new Reservation({
                _id: mongoose.Types.ObjectId(),
                room: req.body.roomId,
                beginDate: req.body.beginDate,
                endDate: req.body.endDate,
                paid: req.body.paid
            });
            return reservation.save(); 
        })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Reservation stored.',
                createdReservation: {
                    id: result._id,
                    room: result.room,
                    beginDate: result.beginDate,
                    endDate: result.endDate,
                    paid: result.paid
                },
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/reservations/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });    
}

exports.reservations_get_one = (req,res,next) => {
    Reservation.findById(req.params.reservationId)
    .select('-__v')
    .populate('room', 'price')
    .exec()
    .then(reservation => {
        if (!reservation) {
            return res.status(404).json({
                message: 'Reservation not found.'
            });
        }
        res.status(200).json({
            reservation: reservation,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/reservations'
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.reservations_delete = (req,res,next) => {
    Reservation.findOne({ _id:req.params.reservationId})
    .exec()
    .then(reservation => {
        if(reservation.paid){
            return res.status(404).json({
                message: 'Reservation has already been paid.'
            }); 
        }
    });
    Reservation.remove({ _id:req.params.reservationId })
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'Reservation deleted.',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/reservations'
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}