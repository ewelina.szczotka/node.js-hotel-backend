const Room = require('../models/room');
const mongoose = require('mongoose');


exports.rooms_get_all = (req,res,next) => {
    Room.find()
    .select('-__v')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            rooms: docs.map(doc => {
                return {
                    beds: doc.beds,
                    price: doc.price,
                    id: doc.id,
                    request: {
                        type: 'GET',
                        url:'http://localhost:3000/rooms/' + doc.id
                    }
                }
            })
        };
        res.status(200).json(response);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.rooms_post = (req,res,next) => {
    const room = new Room({
        _id: new mongoose.Types.ObjectId(),
        beds: req.body.beds,
        price: req.body.price
    });
    room.save().then(result => {
        console.log(result);
        res.status(201).json({
            message: 'Added room successfully.',
            addedRoom: {
                beds: result.beds,
                price: result.price,
                id: result.id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/rooms/' + result._id
                }
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.rooms_get_one = (req,res,next) => {
    const id = req.params.roomId;
    Room.findById(id)
    .select('-__v')
    .exec()
    .then(doc => {
        if(doc){
            res.status(200).json({
                room: doc,
                request: {
                    type: 'GET',
                    description: 'GET_ALL_ROOMS',
                    url: 'http://localhost:3000/rooms/'
                }
            });
        } else {
            res.status(404).json({message: "No valid entry found."});
        }  
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.rooms_patch = (req,res,next) => {
    const id = req.params.roomId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Room.update({_id: id}, { $set: updateOps })
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'Room updated.',
            request: {
                type: 'GET',
                url: 'http://localhost:3000/rooms/' + id
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.rooms_delete = (req,res,next) => {
    const id = req.params.roomId;
    Room.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'Room deleted.',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/rooms/',
                body: { beds: 'Number', price: 'Number'}
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}