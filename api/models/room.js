const mongoose = require('mongoose');

const roomSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    beds: { type: Number, required: true },
    price: { type: Number, required: true }
});


module.exports = mongoose.model('Room', roomSchema);