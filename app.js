const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');


const userRoutes = require('./api/routes/users');
const roomRoutes = require('./api/routes/rooms');
const reservationRoutes = require('./api/routes/reservations');
const paymentRoutes = require('./api/routes/payment');

const ejs = require('ejs');
app.set('view engine', 'ejs');

app.get('/', (req, res) => res.render('index'));

mongoose.connect('mongodb+srv://Ewelina:'+ process.env.MONGO_ATLAS_PW +'@hotel-22vcl.mongodb.net/test?retryWrites=true', {useNewUrlParser: true});

app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
 
app.use((req,res,next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if(req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Method', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

app.use('/users', userRoutes);
app.use('/rooms', roomRoutes);
app.use('/reservations', reservationRoutes);
app.use('/', paymentRoutes);

app.use((req,res,next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})

app.use((error,req,res,next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;